<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dittbygg');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g|Q@$?z[I92)3LQ|<<2-+z:$Y~%+d-<v60~u%1W`<+o2?5Qh+J]DaZm P.0@^!5r');
define('SECURE_AUTH_KEY',  '3desCnV$p `;(TK;N9=w~lm#b/mYOwwBJPFHLk,UJHlRKv]16Q:i)nZ+FvwK:jN*');
define('LOGGED_IN_KEY',    '3174 [YNE$^kqbT^TIs><6q1V*8m3j^wP>kcGXuBpurY$A~+qiD0+uS>qp7G%,Q,');
define('NONCE_KEY',        '`~9h%b?$v}_KAFtaQ9P?@lF:2e-&dv$B@-*jeD^,J{:y+dwv9?Rq~S*]Pt3|:@3C');
define('AUTH_SALT',        'LOuLDg8?tWM+VN?m+5&$*ZPtAg nj]e_!YgmPUfQ0Ue^Z+bINSZ`!p<~+c4~x~%+');
define('SECURE_AUTH_SALT', 'Pk2ZjX/ D98#|H<qm5e}BD;z AQDFG-wp.*rC!E&g}Lf/H]n-##j?[-RvOlur0wz');
define('LOGGED_IN_SALT',   'l`dJ#N+;kA|l:XrL+o/,6YPr,i}jtM.|(UP;oF3(+bL%)NbqD~F?>]k/F-%BZ/Vp');
define('NONCE_SALT',       '~JFKd,8S}VpERc`GO=|6@?Ywltw<|^fntVP+,+2snOU-9}-3,0>XW%b|TiG$,JKa');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
